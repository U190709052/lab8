package shapes3d;

import shapes2d.Square;

public class Cube extends Square {

    public Cube(double side) {
        super(side);
    }

    public double Volume() {
        return super.area() * super.square_side;
    }


    public double area(){
        return super.area()*6;


    }

    public String toString(){

        return "kenar:"+square_side;
    }
}
