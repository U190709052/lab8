package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {

    public Cylinder(double radius_circle, double Cylinder_height) {
        super(radius_circle);
        this.Cylinder_height = Cylinder_height;
    }

    public double Cylinder_height;

    public double area(){
        return (super.area()*2)+(2*super.radius_circle*Math.PI*Cylinder_height);
    }

    public double volume(){
        return super.area()*Cylinder_height;
    }


    public String toString(){

        return "yaricap:"+radius_circle+" "+"yükseklik:"+Cylinder_height;
    }
}