package shapes2d;

public class Square {

    public double square_side;

    public Square(double square_side) {
        this.square_side = square_side;
    }

    public double area(){
        return square_side*square_side;
    }

    public String toString(){
        return "kenar:"+square_side;
    }

}