package shapes2d;

public class Circle {

    public double radius_circle;

    public Circle(double radius_circle) {
        this.radius_circle = radius_circle;
    }

    public double area(){
        return Math.PI*radius_circle*radius_circle;
    }

    public String toString(){

        return "yaricap:"+radius_circle;
    }

}